/**
 * Go2d 命名空间
 * @namespace
 * @property {string} version Go2d 版本号
 */
var go2d = {
	version: '@VERSION'
};
